﻿using EjercicioTalper.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EjercicioTalper.Controllers
{
    public class CaptureController : Controller
    {
        // GET: Capture
        public ActionResult Add()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Add(Models.ViewModels.ProspectoViewModel model, HttpPostedFileBase[] file) 
        {
            DbTransaction transaction;
            try
            {
                model.Documentos = getDocuments(file);
                using (Models.EjercicioTalperEntities db = new Models.EjercicioTalperEntities())
                {
                    db.Database.Connection.Open();
                    transaction = db.Database.Connection.BeginTransaction();
                    db.Database.UseTransaction(transaction);
                    try
                    {
                        Models.Prospectos prospecto = new Models.Prospectos();
                        prospecto.Nombre = model.Nombre.ToUpper();
                        prospecto.ApellidoPaterno = model.ApellidoPaterno.ToUpper();
                        prospecto.ApellidoMaterno = model.ApellidoMaterno.ToUpper();
                        prospecto.Calle = model.Calle.ToUpper();
                        prospecto.Numero = model.Numero.ToUpper();
                        prospecto.Colonia = model.Colonia.ToUpper();
                        prospecto.CodigoPostal = model.CodigoPostal;
                        prospecto.Telefono = model.Telefono.ToUpper();
                        prospecto.RFC = model.RFC.ToUpper();
                        prospecto.Estatus = "REVISIÓN";
                        db.Prospectos.Add(prospecto);
                        db.SaveChanges();
                        foreach (var document in model.Documentos)
                        {
                            Models.Documentos documento = new Models.Documentos();
                            documento.FKProspecto = prospecto.PKProspecto;
                            documento.Nombre = document.Nombre;
                            documento.Archivo = document.Archivo;
                            db.Documentos.Add(documento);
                            db.SaveChanges();
                        }
                        transaction.Commit();
                        db.Database.Connection.Close();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        db.Database.Connection.Close();
                        ViewBag.ErrorMessage = "Error al Insertar " + ex.Message;
                        Console.WriteLine("Error en: " + ex);
                    }
                   
                }
                ViewBag.Message = "Registro Exitoso";
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Error al Insertar " + ex.Message ;
                Console.WriteLine("Error en: "+ex);
                return View();
            }
        }

        private List<DocumentoViewModel> getDocuments(HttpPostedFileBase[] files)
        {
            List<DocumentoViewModel> documents = new List<DocumentoViewModel>();
            
            foreach (var file in files)
            {
                DocumentoViewModel document = new DocumentoViewModel();
              if(file!=null)
                if (file.ContentLength > 0)
                {
                    using (var ms = new MemoryStream())
                    {
                        file.InputStream.CopyTo(ms);                       
                        document.Nombre = Path.GetFileName(file.FileName);
                        document.Archivo = ms.ToArray();
                        documents.Add(document);
                    }
                }
                }
            return  documents;
        }

    }
}