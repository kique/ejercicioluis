﻿using EjercicioTalper.Models.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace EjercicioTalper.Controllers
{
    public class ListProspectsController : Controller
    {
        // GET: ListProspects
        public ActionResult MostrarProspectos()
        {
            List<Models.ViewModels.ProspectoViewModel> listaProspecto = new List<Models.ViewModels.ProspectoViewModel>();
            using (Models.EjercicioTalperEntities db = new Models.EjercicioTalperEntities())
            {
                try
                {
                    listaProspecto = (from p in db.Prospectos
                                      select new Models.ViewModels.ProspectoViewModel
                                      {
                                          PKProspecto = p.PKProspecto,
                                          Nombre = p.Nombre,
                                          ApellidoPaterno = p.ApellidoPaterno,
                                          ApellidoMaterno = p.ApellidoMaterno,
                                          Calle = p.Calle,
                                          Numero = p.Numero,
                                          Colonia = p.Colonia,
                                          CodigoPostal = (int)p.CodigoPostal,
                                          Telefono = p.Telefono,
                                          RFC = p.RFC,
                                          Estatus = p.Estatus,
                                          Observaciones = p.Observaciones,
                                          Documentos = (from d in p.Documentos
                                                        select new Models.ViewModels.DocumentoViewModel
                                                        {
                                                            FKProspecto = (int)d.FKProspecto,
                                                            Nombre = d.Nombre,
                                                            Archivo = d.Archivo
                                                        }).ToList()

                                      }).ToList();
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = "Error al Insertar " + ex.Message;
                    Console.WriteLine("Error en: " + ex);
                }

            }
            ViewBag.Message = "Registro Exitoso";
            return View(listaProspecto);
        }

        [HttpGet]
        public object detalles(int PK)
        {
           ProspectoViewModel prospecto = new ProspectoViewModel();
            using (Models.EjercicioTalperEntities db = new Models.EjercicioTalperEntities())
            {
                try
                {
                    prospecto = (from p in db.Prospectos.Where(pr => pr.PKProspecto == PK)
                                      select new Models.ViewModels.ProspectoViewModel
                                      {
                                          PKProspecto = p.PKProspecto,
                                          Nombre = p.Nombre,
                                          ApellidoPaterno = p.ApellidoPaterno,
                                          ApellidoMaterno = p.ApellidoMaterno,
                                          Calle = p.Calle,
                                          Numero = p.Numero,
                                          Colonia = p.Colonia,
                                          CodigoPostal = (int)p.CodigoPostal,
                                          Telefono = p.Telefono,
                                          RFC = p.RFC,
                                          Estatus = p.Estatus,
                                          Observaciones = p.Observaciones,
                                          Documentos = (from d in p.Documentos
                                                        select new Models.ViewModels.DocumentoViewModel
                                                        {
                                                            FKProspecto = (int)d.FKProspecto,
                                                            Nombre = d.Nombre,
                                                            Archivo = d.Archivo
                                                        }).ToList()

                                      }).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = "Error al Insertar " + ex.Message;
                    Console.WriteLine("Error en: " + ex);
                }

            }
            return JsonConvert.SerializeObject(prospecto);
        }

    }
}