﻿using EjercicioTalper.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EjercicioTalper.Controllers
{
    public class EvaluateController : Controller
    {
        enum Estatus {AUTORIZADO,RECHAZADO,REVISIÓN};
        // GET: Evaluate
        public ActionResult Index()
        {
            List<Models.ViewModels.ProspectoViewModel> listaProspecto = new List<Models.ViewModels.ProspectoViewModel>();
            using (Models.EjercicioTalperEntities db = new Models.EjercicioTalperEntities())
            {
                try
                {
                    listaProspecto = db.Prospectos.Where(p=>p.Estatus.Equals(Estatus.REVISIÓN.ToString())).
                                      Select(p=> new Models.ViewModels.ProspectoViewModel
                                      {
                                          PKProspecto = p.PKProspecto,
                                          Nombre = p.Nombre,
                                          ApellidoPaterno = p.ApellidoPaterno,
                                          ApellidoMaterno = p.ApellidoMaterno
                                      }).ToList();
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = "Error al Insertar " + ex.Message;
                    Console.WriteLine("Error en: " + ex);
                }

            }
            ViewBag.Message = "Registro Exitoso";
            return View(listaProspecto);
        }

        [HttpPost]
        public JsonResult SendEvaluated(Evaluate data)
        {
            using (Models.EjercicioTalperEntities db = new Models.EjercicioTalperEntities())
            {
                try
                {
                    Models.Prospectos prospecto = new Models.Prospectos();
                    prospecto = db.Prospectos.Where(p => p.PKProspecto == data.PKProspecto).FirstOrDefault();
                    if (data.Estatus.ToUpper().Equals(Estatus.AUTORIZADO.ToString()))
                        prospecto.Estatus = Estatus.AUTORIZADO.ToString();
                    else
                    {
                        prospecto.Estatus = Estatus.RECHAZADO.ToString();
                        prospecto.Observaciones = data.Observaciones.ToUpper();
                    }
                    db.SaveChanges(); 
                    return Json("'Success':'true'");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error en: " + ex);
                    return Json(String.Format("'Success':'false','Error':'Ha habido un error al insertar el registro.'"));
                }

            }
        }
    }
}