﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EjercicioTalper.Models.ViewModels
{
    public class DocumentoViewModel
    {
        public int FKProspecto { get; set; }
        public string Nombre { get; set; }
        public byte[] Archivo { get; set; }
    }
}