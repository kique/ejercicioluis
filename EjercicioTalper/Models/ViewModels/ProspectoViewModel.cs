﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EjercicioTalper.Models.ViewModels
{
    public class ProspectoViewModel
    {
        public int PKProspecto { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Calle { get; set; }
        public string Numero { get; set; }
        public string Colonia { get; set; }
        public int CodigoPostal { get; set; }
        public string Telefono { get; set; }
        public string RFC { get; set; }
        public string Estatus { get; set; }
        public string Observaciones { get; set; }
        public List<DocumentoViewModel> Documentos { get; set; }
    }
}