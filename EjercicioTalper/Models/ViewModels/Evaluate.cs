﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EjercicioTalper.Models.ViewModels
{
    public class Evaluate
    {
        public int PKProspecto { get; set; }
        public string Estatus { get; set; }
        public string Observaciones { get; set; }
    }
}