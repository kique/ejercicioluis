﻿let documentos;
function viewInfo(pk) {
    let modal = document.getElementById('modal');
    let flex = document.getElementById('flex');
    url = '/ListProspects/detalles?PK=' + pk;
    $.ajax({
        crossDomain: true,
        async: false,
        url: url,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            detalles = JSON.parse(response);
        },
        error: function (error) {
            //debugger;
            console.log(error);
        }
    });
    document.getElementById("name").innerHTML = detalles.Nombre + ' ' + detalles.ApellidoPaterno + ' ' + detalles.ApellidoMaterno;
    document.getElementById("txtRFC").value = detalles.RFC;
    document.getElementById("txtEstatus").value = detalles.Estatus;
    document.getElementById("txtCalle").value = detalles.Calle;
    document.getElementById("txtNumero").value = detalles.Numero;
    document.getElementById("txtColonia").value = detalles.Colonia;
    document.getElementById("txtCodigoPostal").value = detalles.CodigoPostal;
    document.getElementById("txtTelefono").value = detalles.Telefono;
    document.getElementById("txtObservaciones").value = detalles.Observaciones;
    debugger;
    if (detalles.Estatus == "RECHAZADO") {
        document.getElementById("lblObservaciones").style.visibility = 'visible';
        document.getElementById("txtObservaciones").style.visibility = 'visible';
    }

    documentos = detalles.Documentos;
    for (var i = 0; i < detalles.Documentos.length; i += 1) {
        var documento = $('#templete-documentos').html();
        doc = documento.replace('x', i).replace('N', documentos[i].Nombre);
        $('#goals-documents').append(doc);

    }


    this.addEventListener('click', function () {
        modal.style.display = 'block';
    });

    window.addEventListener('click', function (e) {
        if (e.target == flex) {
            modal.style.display = 'none';
        }
    });
}
function actDownload(index) {
    download(documentos[index].Archivo, documentos[index].Nombre, "application/pdf")
}
function cerrar() {
    window.location.reload();
    this.addEventListener('click', function () {
        modal.style.display = 'none';
    });
}

function download(strData, strFileName, strMimeType) {
    var D = document, A = arguments, a = D.createElement("a"),

        d = A[0], n = A[1], t = A[2] || "application/pdf";

    var newdata = "data:" + strMimeType + ";base64," + escape(strData);
    a.href = newdata;
    if ('download' in a) {

        a.setAttribute("download", n);

        a.innerHTML = "downloading...";

        D.body.appendChild(a);

        setTimeout(function () {

            var e = D.createEvent("MouseEvents");


            e.initMouseEvent("click", true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null

            );

            a.dispatchEvent(e);

            D.body.removeChild(a);

        }, 66);

        return true;

    };

}